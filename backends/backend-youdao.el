;;; backend-youdao.el --- Translating backend from youdao. -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Free Software Foundation, Inc.

;;; Commentary:



;;; Code:

(defun emacs-translate.backend.youdao/translate (text &optional async)
  )



(provide 'backend-youdao)

;;; backend-youdao.el ends here
