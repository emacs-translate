;;; emacs-translate.el --- Emacs translate front-end framework supports customize variant translating backends. -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Free Software Foundation, Inc.

;;; Commentary:

;; This extension provides a UI front-end for variant translating backends.

;;; Code:

(require 'core)
(require 'ui)

(defgroup emacs-translate nil
  "Emacs translate front-end framework supports customize variant translating backends."
  :prefix "emacs-translate-"
  :group 'emacs-translate)

(defcustom emacs-translate-backends-list '("youdao" "baidu-translate" "bing-translate" "google-translate" "DeepL")
  "A list of translating backend strings."
  :type 'list
  :safe #'listp
  :group 'emacs-translate)

(defcustom emacs-translate-default-backend 'bing-translate
  "Specify the default translating backend."
  :type 'symbol
  :safe #'symbolp
  :group 'emacs-translate)

(defcustom emacs-translate-mode-map-prefix (kbd "C-c C-t")
  "Specify emacs-translate keybindings prefix before loading."
  :type 'kbd
  :group 'emacs-translate)

(defvar emacs-translate-mode-map
  (let ((map (make-sparse-keymap)))
    (unless (keymapp map)
      (global-set-key emacs-translate-mode-map-prefix map)
      (define-key emacs-translate-mode-map-prefix (kbd "t") 'emacs-translate.ui/translate-at-point)
      (define-key emacs-translate-mode-map-prefix (kbd "r") 'emacs-translate.ui/translate-region)
      (define-key emacs-translate-mode-map-prefix (kbd "b") 'emacs-translate.ui/translate-buffer)
      (define-key emacs-translate-mode-map-prefix (kbd "p") 'emacs-translate.ui/translate-paragraph-by-paragraph-overlay)
      (define-key emacs-translate-mode-map-prefix (kbd "P") 'emacs-translate.ui/translate-paragraph-by-paragraph-insert)
      (define-key emacs-translate-mode-map-prefix (kbd "C-p") 'emacs-translate.ui/translate-popup))
    map)
  "emacs-translate-mode map.")

(define-minor-mode emacs-translate-mode
  "A global minor mode to toggle extension `emacs-translate'."
  :require 'emacs-translate
  :init-value nil
  :lighter " emacs-translate"
  :group 'emacs-translate
  :keymap emacs-translate-mode-map
  (if emacs-translate-mode
      (progn
        (dolist (backend emacs-translate-backends-list)
          (require (intern (concat "backend-" backend)))))))



(provide 'emacs-translate)

;;; emacs-translate.el ends here
