;;; ui.el --- Provides UI framework for translating. -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Free Software Foundation, Inc.

;;; Commentary:



;;; Code:

(defun emacs-translate.ui/translate-at-point (&optional backend)
  (interactive "p")
  (if current-prefix-arg
      (setq backend (intern (completing-read "Select emacs-translate.el translating backend: "
                                             emacs-translate-backends-list))))
  (if-let ((text (thing-at-point 'word)))
      (emacs-translate.core/translate text backend)
    (error "Nothing at point.")))

(defun emacs-translate.ui/translate-region (&optional backend)
  (interactive "p")
  (if current-prefix-arg
      (setq backend (intern (completing-read "Select emacs-translate.el translating backend: "
                                             emacs-translate-backends-list))))
  (if (region-active-p)
      (let ((text (buffer-substring-no-properties (region-beginning) (region-end))))
        (emacs-translate.core/translate text backend))
    (error "No region selected!")))

(defun emacs-translate.ui/translate-buffer (&optional backend)
  (interactive "p")
  (if current-prefix-arg
      (setq backend (intern (completing-read "Select emacs-translate.el translating backend: "
                                             emacs-translate-backends-list))))
  (if-let ((text (buffer-substring-no-properties (point-min) (point-max))))
      (emacs-translate.core/translate text backend)
    (error "Buffer is empty!")))

(defun emacs-translate.ui/translate-paragraph-by-paragraph-overlay (&optional backend)
  (interactive "p")
  (if current-prefix-arg
      (setq backend (intern (completing-read "Select emacs-translate.el translating backend: "
                                             emacs-translate-backends-list))))
  )

(defun emacs-translate.ui/translate-paragraph-by-paragraph-insert (&optional backend)
  (interactive "p")
  (if current-prefix-arg
      (setq backend (intern (completing-read "Select emacs-translate.el translating backend: "
                                             emacs-translate-backends-list))))
  )



(provide 'ui)

;;; ui.el ends here
