;;; core.el --- Provides core API of translating. -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Free Software Foundation, Inc.

;;; Commentary:



;;; Code:

(defcustom emacs-translate.core/debug-p nil
  "Whether display emacs-translate debugging info in buffer `*emacs-translate*'."
  :type 'boolean
  :safe #'booleanp
  :group 'emacs-translate)

(defcustom emacs-translate.core/async-p t
  "Whether translate asynchronous or synchronous."
  :type 'boolean
  :safe #'booleanp
  :group 'emacs-translate)

(defun emacs-translate.core/--sync-translate (text &optional backend)
  "Non-async translating core API function."
  (funcall (intern (format "emacs-translate.backend.%s/translate" backend)) text))

(defun emacs-translate.core/--async-translate (text &optional backend)
  "Async translating core API function."
  (funcall (intern (format "emacs-translate.backend.%s/translate" backend)) text))

(defun emacs-translate.core/translate (text &optional backend async)
  "The main entry core API function for invoke translate."
  (let ((backend (or backend emacs-translate-default-backend)))
    (if (or async emacs-translate.core/async-p)
        (emacs-translate.core/--async-translate backend text)
      (emacs-translate.core/--sync-translate backend text))))



(provide 'core)

;;; core.el ends here
